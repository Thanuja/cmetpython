# Tesing print statements
#print "Hello python" # This is python 2.0, doesnt work with 3.0
print ("Hello python 3.7")

# combine multiple strings with +
print ("This" + "is a string")

# Errors in print statements
'''
print("How do you make a hot dog stand?') # quote missmatch
print(You take away its chair!) # no quotes
'''

# Variables to define that are subjected to change
greeting_message = "Welcome to Codecademy!"
current_excercise = 5
todays_date = "10-9-2018" # Is there a function to get todays date!!

# Arithmetic operations;
mirthful_addition = 12381 + 91817
amazing_subtraction = 981 - 312
trippy_multiplication = 38 * 902
happy_division = 540 / 45
sassy_combinations = 129 * 1345 + 120 / 6 - 12

is_this_number_odd = 15 % 2
is_this_number_divisible_by_seven = 133 % 7

product =  3 * 5
remainder = 1398 % 11
'''
Qs:
    Why didn’t my product display on the screen?
        You need print (str(product))
    There are variables defined in the instructions, can I use those in my code?
        You need to define the variables before using them
    Does my variable name need to match the math I’m doing
        It would be helpful to the reader.
'''


### Updating variables!
january_to_june_rainfall = 1.93 + 0.71 + 3.53 + 3.41 + 3.69 + 4.50
annual_rainfall = january_to_june_rainfall

july_rainfall = 1.05
annual_rainfall += july_rainfall

august_rainfall = 4.91
annual_rainfall += august_rainfall

september_rainfall = 5.16
october_rainfall = 7.20
november_rainfall = 5.06
december_rainfall = 4.06

annual_rainfall += (september_rainfall + october_rainfall + november_rainfall + december_rainfall)
'''
Qs: 
    How does code academy check my answers?
    
    How does Python reassign a variable’s value?
    
    Why would we want to change the value of the variable?
'''

### Comments
city_name = "St. Potatosburg"
# comment
city_pop = 340000
'''
Qs;
    why should i write comments in the code?
        Good to write comments for potentially ambiguous statements, variables code snipetts
        
    Where can i write comments?
        anywhere suitable
        
    
'''

### Numbers
# Integers; no decimal point
# floating point numbers
# e notation; e indicates power of 2
num = 1.5e2 ## 150
cucumbers = 2
price_per_cucumber=3.25
total_cost = cucumbers * price_per_cucumber
print(total_cost)
print(type(total_cost))

'''
Qs:
    why does total_cost becomes a float?
        integer * float  = float
    
'''

### Quotients
quotient = 6/2 # 3
quotient = 7/2 # 3 due to round up
quotient1 = 7./2
# the value of quotient1 is 3.5
quotient2 = 7/2.
# the value of quotient2 is 3.5
quotient3 = 7./2.
# the value of quotient3 is 3.5
quotient1 = float(7)/2

cucumbers = 100
num_people = 6
whole_cucumbers_per_person = cucumbers/num_people
print(whole_cucumbers_per_person)

float_cucumbers_per_person = float(cucumbers)/num_people
print(float_cucumbers_per_person)

'''
Qs: 
    
Log In
    Why is it invalid to add a period after a variable name to make it a float?
        it will change the syntax, use the function float()
        
    How does float( ) work?
        built in function?? How??
        
'''

### Multi line strings
# Use triple quotes

address_string = """136 Whowho Rd
Apt 7
Whosville, WZ 44494"""

haiku = """
The old pond,
A frog jumps in:
Plop!
"""

'''
Qs:
    Two types of comments
        single line
        multi line for documentation, commenting a block of code
    
'''

### Booleans
# Hi! I'm Maria and I live in script.py.
# I'm an expert Python coder.
# I'm 21 years old and I plan to program cool stuff forever.

a = True
b = False
age_is_12=False
name_is_maria=True

'''
Qs:
    
'''

### Type Error / Type conversion
float_1 = 0.25
float_2 = 40.0
age = 13
print ("I am " + str(age) + " years old!")


number1 = "100"
number2 = "10"

string_addition = number1 + number2
#string_addition now has a value of "10010"

int_addition = int(number1) + int(number2)
#int_addition has a value of 110

string_num = "7.5"
#print int(string_num) # Type Error
print (float(string_num))

product = float_1 * float_2
big_string = "The product was " + str(product)

'''
Qs;     

    is it possible to do more than 1 type conversion at once
        yes str(int(some_variable))
    Does str() change the value stored in the variable
        No
        
'''
