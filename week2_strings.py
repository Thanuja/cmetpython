
# Sample string variables;
from pip._vendor.distlib.compat import raw_input
from datetime import datetime

name = "Ryan"
age = "19"
food = "cheese"

brian =  "Hello life!"

# Special characters need to be escaped using a backslash \
# 'This isn\'t flying, this is falling with style!'
'''
Other special characters
    \\, which will display the backslash
    \t, which displays a tab-length spacing
    \n, which displays a new line
    and \' and \", which display the single and double quotes

'''

## Access strings by indexes
fifth_letter = "MONTY"[4]
print (fifth_letter)
'''
Qs: 
    Why start from 0?
    Dijkstra, gives a great mathematical explanation for this! It basically comes down to optimization, 
    which is a common reason behind computer system design choices like this. 
    Dijkstra says, “when starting with subscript 1, the subscript range 1 ≤ i < N+1; starting with 0, 
    however, gives the nicer range 0 ≤ i < N,” in his paper called “Why numbering should start at zero.”

'''

# String methods; perform tasks on Strings
parrot="Norwegian Blue"
print (len(parrot))
print (parrot.lower())
print (parrot.upper())
pi = 3.14
print (str(pi))

# String concatenation
print ("The value of pi is around " + str(3.14))
'''
Qs:
    Do these methods actually change the original variable?
    NO
    Unless you assign the variable to be itself with a string method modifying it, then it will remain the same
    
    Dot notation (upper(), lower()). vs Other methods (str(), len())
        .() methods act on objects, other at not attached to an object. 
        Python is object oriented???
   
    Why Python doesnt convert number to string automatically?
        explicit is better than implicit. 
        easy to debug when something goes wrong     
'''

#String formatting
string_1 = "Camelot"
string_2 = "place"

print("Let's not go to %s. 'Tis a silly %s." % (string_1, string_2))

day = 6
print("03 - %s - 2019" %(day))
# 03 - 6 - 2019
print("03 - %02d - 2019" %(day)) # a comma wont work!!
# 03 - 06 - 2019

#raw_input seems like a custom function?. How to get user input in python??
name = raw_input("What is your name? ")
quest = raw_input("What is your quest? ")
color = raw_input("What is your favorite color? ")

print ("Ah, so your name is %s, your quest is %s, " \
"and your favorite color is %s." %(name, quest, color))

# input is for integers
# What happens when a character is given??, Nothin!!!
age = input("What is your age? ")
print("Your age is: ", age)

#####################################################################################

now = datetime.now()
print (now)
print (type(now)) # of type datetime object

current_year = now.year
current_month = now.month
current_day = now.day

print(now.year)
print(now.month)
print(now.day)

print(now.strftime('%B'))
print('%02d-%02d-%04d' % (now.month, now.day, now.year))
print('%02d/%02d/%04d' % (now.month, now.day, now.year))

# Extracting time.
print(now.hour)
print(now.minute)
print(now.second)

print('%02d:%02d:%04d' % (now.hour, now.minute, now.second))

