from pip._vendor.distlib.compat import raw_input


def clinic():
    print("You've just entered the clinic!")
    print("Do you take the door on the left or the right?")
    answer = raw_input("Type left or right and hit 'Enter'.").lower()
    if answer == "left" or answer == "l":
        print("This is the Verbal Abuse Room, you heap of parrot droppings!")
    elif answer == "right" or answer == "r":
        print("Of course this is the Argument Room, I've told you that already!")
    else:
        print("You didn't pick left or right! Try again.")
        clinic()

clinic()

## Comparators
'''
Comparators check if a value is (or is not) equal to, greater than (or equal to), or less than (or equal to) another value.
'''
# Make me true!
bool_one = 3 < 5  # We already did this one for you!
# Make me false!
bool_two = 1 == 2
# Make me true!
bool_three = 5 != -5
# Make me false!
bool_four = 6 > 7
# Make me true!
bool_five = 7 < 10

'''
and, or, not
'''
bool_one = False and False
bool_two = -(-(-(-2))) == -2 and 4 >= 16 ** 0.5
bool_three = 19 % 4 != 300 / 10 / 10 and False
bool_four = -(1 ** 2) < 2 ** 0 and 10 % 10 <= 20 - 10 * 2
bool_five = True and True

'''
or:
'''
bool_one = 2 ** 3 == 108 % 100 or 'Cleese' == 'King Arthur'
bool_two = True or False
bool_three = 100 ** 0.5 >= 50 or False
bool_four = True or True
bool_five = 1 ** 100 == 100 ** 1 or 3 * 2 * 1 != 3 + 2 + 1

'''
Order of boolean evaluation
1. Not
2. and
3. or
Parentheses () ensure your expressions are evaluated in the order you want
'''

'''
conditional statements
'''

if 8 > 9:
  print("I don't printed!")
else:
  print("I get printed!")

answer = "'Tis but a scratch!"

def black_knight():
    if answer == "'Tis but a scratch!":
        return True
    else:
        return  False      # Make sure this returns False

def french_soldier():
    if answer == "Go away, or I shall taunt you a second time!":
        return True
    else:
        return  False      # Make sure this returns False

if 8 > 9:
  print("I don't get printed!")
elif 8 < 9:
  print("I get printed!")
else:
  print("I also don't get printed!")


def greater_less_equal_5(answer):
    if answer > 5:
        return 1
    elif answer < 5:
        return -1
    else:
        return 0

print (greater_less_equal_5(4))
print (greater_less_equal_5(5))
print (greater_less_equal_5(6))


# Complete the if and elif statements!
def grade_converter(grade):
    if grade >= 90:
        return "A"
    elif grade <= 89 and grade >= 80:
        return "B"
    elif grade <= 79 and grade >= 70:
        return "C"
    elif grade <= 69 and grade >= 65:
        return "D"
    else:
        return "F"


# This should print an "A"
print(grade_converter(92))

# This should print a "C"
print(grade_converter(70))

# This should print an "F"
print(grade_converter(61))




