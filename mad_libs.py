"""
This program generates passages that are generated in mad-lib format
Author: Katherin
"""

# The template for the story
from pip._vendor.distlib.compat import raw_input

STORY = "This morning _ woke up feeling _. 'It is going to be a _ day!' Outside, a bunch of _s were protesting to keep _ in stores. They began to _ to the rhythm of the _, which made all the _s very _. Concerned, _ texted _, who flew _ to _ and dropped _ in a puddle of frozen _. _ woke up in the year _, in a world where _s ruled the world."

print("Mad Libs has started")
name = raw_input("Enter a name: ")
adjectives = [str(n) for n in raw_input('Enter 3 adjectives spacing each: ').split()]
#print len(adjectives)
#print(adjectives[1])

verb = raw_input("Enter a verb: ")

nouns = []
counter=0
while counter < 2:
  temp = raw_input("Enter a noun: ")
  nouns.append(temp)
  counter += 1

animal = raw_input("Enter an animal: ")
food = raw_input("enter food item: ")
fruit = raw_input("enter fruit: ")
superhero = raw_input("enter a superhero name: ")
country = raw_input ("Enter a country: ")
dessert = raw_input ("Enter a dessert: ")
year = raw_input ("Enter a year: ")

STORY = "This morning %s woke up feeling %s. 'It is going to be a %s day!' Outside, a bunch of %ss were protesting to keep %s in stores. They began to %s to the rhythm of the %s, which made all the %ss very %s. Concerned, %s texted %s, who flew %s to %s and dropped %s in a puddle of frozen %s. %s woke up in the year %s, in a world where %ss ruled the world."

print ("This morning %s woke up feeling %s. 'It is going to be a %s day!' Outside, a bunch of %ss were protesting to keep %s in stores. They began to %s to the rhythm of the %s, which made all the %ss very %s. Concerned, %s texted %s, who flew %s to %s and dropped %s in a puddle of frozen %s. %s woke up in the year %s, in a world where %ss ruled the world." %(name, adjectives[0], adjectives[1], animal, food, verb, nouns[0], fruit,  adjectives[2], name, superhero, name, country, name, dessert, name, year, nouns[1]))

