dna_sequence = 'acgatagc' # length = 8
'''
codon: sequence of 3 DNA nucleotides corresponding to amino acids
or stop signal for protein synthesis
'''
last_codon_start = len(dna_sequence) - 2
protein = ""

for condon_start in range(0,last_codon_start,3):
    codon = dna_sequence[condon_start:condon_start+3]

    # genetic_code is the dictionary
    amino_acid = genetic_code.get(codon.upper(), 'X')
    protein = protein + amino_acid


##------------------------------------

foo = []
foo.append(bar)

##------------------------------------

'''
The convention is to use i, j, and ii, and jj, kk, for searches through the code!!!???
e.g,,
    when searching, a single 'i', could pop many instances
    ii, jj, may just point to the right for loop you are looking for
'''
for i in range(10):
    for j in range(20):
        # do something!
        print(i + j)

## -------------------------

minimum_name_length = 20
species_name = "Homo sapiens"
reading_frames = [1,2,3]
input_file = open('foo.txt')


def calculate_average(scores):
    total = 0
    num_scores = len(scores)
    for score in scores:
        total += score

    return total/num_scores

## -----------------------

word = 'albatross'
uppercase_word = word.upper()
punctuated_word = uppercase_word + "!"

